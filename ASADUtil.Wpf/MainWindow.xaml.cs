﻿using System;
using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Forms;
using ASADUtil.Service;

namespace ASADUtil.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private bool isRangeSliderMouseUp = false;

        ViewModel model = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = model;
        }

        private void ChooseDumpFile_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            // Get the selected file name and display in a TextBox 
            DialogResult result = folderDialog.ShowDialog();
            if ("OK".Equals(result.ToString()))
            {
                // Open document 
                string filename = folderDialog.SelectedPath;
                tbDumpFileName.Text = filename;
            }

        }


        private void ConfirmCloseButton_Click(object sender, RoutedEventArgs e)
        {
            Confirm.IsOpen = false;
        }
        private void ConfirmOpenButton_Click(object sender, RoutedEventArgs e)
        {
            Confirm.IsOpen = true;
        }

        private void ChooseFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.CheckPathExists = true;
            dialog.CheckFileExists = false;

            DialogResult result = dialog.ShowDialog();
            if ("OK".Equals(result.ToString()))
            {
                string fileName = dialog.FileName;
                tbSourceFileName.Text = fileName;
            }
        }

        private void SaveData_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.RestoreDirectory = true;
            dialog.CheckPathExists = true;
            dialog.CheckFileExists = false;

            DialogResult result = dialog.ShowDialog();
            if ("OK".Equals(result.ToString()))
            {
                string fileName = dialog.FileName;
                model.SaveCommand.DoExecute(fileName);
            }
        }

        private void RangeSlider_RangeSelectionChanged(object sender, RangeSelectionChangedEventArgs e)
        {
            if (isRangeSliderMouseUp)
            {
                CustomDiskArea.IsChecked = true;
                isRangeSliderMouseUp = false;
            }
        }

        private void RangeSlider_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            isRangeSliderMouseUp = true;
        }

        private void IntegerUpDown_GotFocus(object sender, RoutedEventArgs e)
        {
            isRangeSliderMouseUp = true;
        }
    }
}
