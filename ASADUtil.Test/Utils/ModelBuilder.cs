using System;
using System.IO;
using System.Reflection;
using Microsoft.Win32.SafeHandles;
using ASADUtil.Service.Model;

namespace ASADUtil.Test.Data
{
	public abstract class Constants {
		public static int TOTAL_SECTORS = 12;
		public static int BAD_SECTORS = 11;
		public static uint SECTOR_SIZE = 512;
		public static long TOTAL_DATA_SIZE = TOTAL_SECTORS * SECTOR_SIZE;

		public static String TEST_DATA_DIR = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/../../test-data/";
		public static String TEST_MERA_DIR = TEST_DATA_DIR + "mera/";

		public static String DATA_GOOD_PATH = TEST_DATA_DIR + "good-data.dat";
		public static String DATA_WRONG_CRC_PATH = TEST_DATA_DIR + "wrong-crc-data.dat"; //bad crc for 3-sector
		public static String DATA_EMPTY_PATH = TEST_DATA_DIR + "empty-data.dat";
		public static String DATA_BREAK_PATH = TEST_DATA_DIR + "break-data.dat";


		public static SafeFileHandle SAFE_FILE_HANDLE = new SafeFileHandle((IntPtr)100, false);
	}

	public class ModelBuilder
	{
		public static Mera buildMera ()
		{
			Mera mera = Mera.Builder.Create ()
				.WithFileName ("test")
				.WithName ("test")
				.WithProductName("test")
				.WithStartDate(DateTime.Now)
				.Build ();

			return mera;
		}

		public static UsbDevice.Builder createDeviceBuilder() 
		{
			UsbDevice.Builder builder = UsbDevice.Builder.Create ()
				.WithBytesPerSector (Constants.SECTOR_SIZE)
				.WithCaption ("Test")
				.WithSize ((ulong) Constants.TOTAL_DATA_SIZE)
				.WithTotalSectors ((uint) Constants.TOTAL_SECTORS)
				.WithDiskRange(0, (ulong) Constants.TOTAL_SECTORS);

			return builder;

		}
	}
}

