using System;
using NUnit.Framework;
using NUnit.Mocks;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using ASADUtil.Service.Disk;
using ASADUtil.Service.Model;
using ASADUtil.Test.Data;
using NSubstitute;
using System.Security.Cryptography;

namespace ASADUtil.Test.Disk
{
	[TestFixture]
	public class DataReaderTest
	{
		private DataReader reader;

		UsbDevice sdCard;
		Mera mera;

		int progressEventCounter;

		[SetUp] 
		public void Init() 
		{
			clearMeraDirectory ();

			progressEventCounter = 1;

			reader = new DataReader ();
			reader.ChangeProgressEvent += HandleChangeProgressEvent;
			reader.ErrorEvent += HandleErrorEvent;

			mera = ModelBuilder.buildMera ();
		}

		private void HandleChangeProgressEvent (object sender, ReaderEventArgs e)
		{
			progressEventCounter--;
		}

		private void HandleErrorEvent (object sender, ReaderEventArgs e)
		{
			Assert.Fail (((Exception)e.Value).Message);
		}

		private void HandleSizeErrorEvent (object sender, ReaderEventArgs e)
		{
			Assert.AreEqual ("Не достаточно места на диске", ((Exception)e.Value).Message);
		}

		[Test] 
		public void cleanTest()
		{
			sdCard = ModelBuilder.createDeviceBuilder ()
				.WithDeviceId(Constants.DATA_EMPTY_PATH)
				.Build();

			ICollection<SafeFileHandle> mountedVolumes; 

			FileStream fs = new FileStream(sdCard.DeviceId, FileMode.Create);
			fs.SetLength(Constants.TOTAL_DATA_SIZE);

			IDiskOperation mockDiskOperation = Substitute.For<IDiskOperation> ();
			mockDiskOperation.OpenAndLockDisk (Arg.Any<String>(), out mountedVolumes).Returns (args => 
				{
					args[1] = new List<SafeFileHandle>();
					return fs.SafeFileHandle;
				});

			mockDiskOperation.MovePointerToPos (Arg.Any<SafeFileHandle>(), Arg.Any<ulong>()).Returns (args => 
				{
					Assert.AreEqual(sdCard.DiskRange.StartPos * (ulong) Constants.SECTOR_SIZE, (ulong) args[1]);
					Console.WriteLine(fs.Position);
					return (uint) fs.Seek((long) ((ulong) args[1]), SeekOrigin.Begin);
				});

			mockDiskOperation.WriteFile (Arg.Any<SafeFileHandle> (), Arg.Any<byte[]> (), Arg.Any<int> ()).Returns (args =>
				{
					fs.Write((byte[]) args[1], 0, (int) args[2]);
					return (int) args[2];
				});

			mockDiskOperation.When (x => x.CloseAndUnlockDisk (Arg.Any<SafeFileHandle> (), Arg.Any<ICollection<SafeFileHandle>> ())).Do (args => 
				{
					fs.Close();
				});

			reader.setDiskOperation (mockDiskOperation);

			reader.clean (sdCard);
			fs = new FileStream (sdCard.DeviceId, FileMode.Open);

			byte[] buff = new byte[sdCard.BytesPerSector];
			for (int i = 0; i < Constants.TOTAL_SECTORS; i++) 
			{
				fs.Read (buff, 0, (int) sdCard.BytesPerSector);
				for (int j = 0; j < sdCard.BytesPerSector; j++) 
				{
					Assert.AreEqual (0xFF, buff [j], "Wrong data at " + i + " " + j);
				}
			}
		}

		[Test]
		public void saveBigSizeTest()
		{
			sdCard = ModelBuilder.createDeviceBuilder ()
				.WithTotalSectors (long.MaxValue)
				.WithDiskRange (0, (ulong) (long.MaxValue/Constants.SECTOR_SIZE))
				.Build();

			reader.ErrorEvent -= HandleErrorEvent;
			reader.ErrorEvent += HandleSizeErrorEvent;

			reader.save(sdCard, Constants.DATA_EMPTY_PATH);

		}

		[Test]
		public void saveTest()
		{
			sdCard = ModelBuilder.createDeviceBuilder ()
				.WithDeviceId(Constants.DATA_GOOD_PATH)
				.WithDiskRange(2, 4)
				.Build();

			File.Delete (Constants.DATA_EMPTY_PATH);

			reader.save(sdCard, Constants.DATA_EMPTY_PATH);

			byte[] hashValue, srcHashVal;

			using (var srcStream = new FileStream (Constants.DATA_GOOD_PATH, FileMode.Open)) 
			{
				byte[] buffer = new byte[sdCard.DiskRange.GetSize () * sdCard.BytesPerSector];
				srcStream.Seek ((long)(sdCard.DiskRange.StartPos * sdCard.BytesPerSector), SeekOrigin.Begin);
				srcStream.Read (buffer, 0, buffer.Length);
				srcHashVal = MD5.Create ().ComputeHash (buffer, 0, buffer.Length);
			}

			using (var stream = File.OpenRead (Constants.DATA_EMPTY_PATH)) 
			{
				hashValue = MD5.Create ().ComputeHash (stream);
			}

			Assert.IsTrue (srcHashVal.SequenceEqual(hashValue), "Source abd distanation files aren't equals");
		}

		[Test]
		public void convertSuccessDataTest ()
		{
			sdCard = ModelBuilder.createDeviceBuilder ()
				.WithDeviceId (Constants.TEST_DATA_DIR + "break-data-sin.dat")
				//.WithDiskRange (2, 7)
				.Build();

			reader.convert (sdCard, mera, Data.Constants.TEST_MERA_DIR);
			Assert.AreEqual (0, progressEventCounter, "Progress steps are incorrect");

			Dictionary<String, int> channelsMap = new Dictionary<string, int>(){
				{"tms_V", 5},
				{"tms_S", 5},

				{"T(cpu)", 4},
				{"tms_Q", 8},
				{"tms_R", 8},
				{"PowerGood", 4},
				{"Vsup", 4},
				{"V(ref_meas)", 4}
			};

			checkMeraFiles ((int) sdCard.DiskRange.GetSize(), channelsMap);
		}


		[Test]
		public void convertBadDataTest ()
		{
			sdCard = ModelBuilder.createDeviceBuilder ()
				.WithDeviceId(Constants.DATA_WRONG_CRC_PATH)
				.Build();

			reader.convert (sdCard, mera, Data.Constants.TEST_MERA_DIR);
			Assert.AreEqual (0, progressEventCounter, "Progress steps are incorrect");

			Dictionary<String, int> channelsMap = new Dictionary<string, int>(){
				{"tms_V", 11},
				{"tms_S", 11},

				{"T(cpu)", 12},
				{"tms_Q", 12},
				{"tms_R", 12},
				{"PowerGood", 8},
				{"Vsup", 8},
				{"V(ref_meas)", 8}
			};

			checkMeraFiles (11, channelsMap);
		}


		[Test]
		public void convertBreakDataTest ()
		{
			sdCard = ModelBuilder.createDeviceBuilder ()
			  	.WithDeviceId(Constants.DATA_BREAK_PATH)
				.WithDeviceId(Constants.TEST_DATA_DIR + "../../data/20130916_6_m.bin")
				.Build();

			using (FileStream fs = File.OpenRead(sdCard.DeviceId))
			{
				sdCard = UsbDevice.deserialize(fs, sdCard.DeviceId);
			}
			sdCard.setDefaultRange();

			sdCard.DataRecord.BytesPerSector = sdCard.BytesPerSector;
			sdCard.DataRecord.Length = DiskOperation.RecordSize(sdCard);
			sdCard.DiskRange.EndPos = sdCard.DataRecord.Length;

			reader.convert (sdCard, mera, Data.Constants.TEST_MERA_DIR);
		}

		[Test]
		public void generateData()
		{
			String fileName = Constants.TEST_DATA_DIR + "good-data-sin.dat";

			CRC16 crc16 = new CRC16();

			using (FileStream fs = File.Open(fileName, FileMode.Open))
			{
				int idx = 0;
				byte [] buffer = new byte[Constants.SECTOR_SIZE];

				for(int i = 0; i<Constants.TOTAL_SECTORS; i++) 
				{
					fs.Seek(i*Constants.SECTOR_SIZE, SeekOrigin.Begin);
					fs.Read(buffer, 0, buffer.Length);

					for(int j=0; j<SdData.ADC_FRAME_COUNT; j++) 
					{
						for(int k=0; k<8; k++){
							double val = 32000 * Math.Sin(Math.PI/SdData.ADC_FRAME_COUNT * idx + Math.PI/16 * k);
							int offset = j*16+2*k+16;
							buffer[offset] = (byte) (((UInt16) val & 0xff00) >> 8);
							buffer[offset + 1] = (byte) ((UInt16) val & 0x00ff);
							//Console.WriteLine("{0}\t{1}\t{2}", j, buffer[offset], buffer[offset+1]);
							Console.WriteLine("{0}\t{1:F2}\t\t{2:x2}-{3:x2}", j, val, buffer[offset], buffer[offset+1]);
						}
						idx++;
					}
					unsafe {
						IntPtr ptr = Marshal.AllocHGlobal(510);
						Marshal.Copy(buffer, 0, ptr, 510);
						byte[] crcData = crc16.ComputeHash((byte*) ptr, 510);
						buffer[510] = crcData[0];
						buffer[511] = crcData[1];
					}
					fs.Seek(i*Constants.SECTOR_SIZE, SeekOrigin.Begin);
					fs.Write(buffer, 0, buffer.Length);
				}
			}
		}

		private void clearMeraDirectory()
		{
			Directory.GetFiles(Data.Constants.TEST_MERA_DIR).ToList().ForEach(File.Delete);
		}

		private void checkMeraFiles(int sectors, Dictionary<String, int> channelsMap = null) 
		{
			foreach (Channel channel in Mera.Channels) 
			{
				for (int i = 0; i < channel.Total; i++)
				{
					string name = string.Format(channel.Mask, channel.Name, i + 1);
					string path = Data.Constants.TEST_MERA_DIR + name + ".dat";

					Assert.True (File.Exists(path), "Unexisting file: " + path);
					FileInfo info = new FileInfo (path);

					long expectedLength;
					if (channelsMap == null) {
						expectedLength = 4 * sectors * SdData.ADC_FRAME_COUNT / channel.Divisor;
					} else {
						expectedLength = 4 * channelsMap [channel.Name] * SdData.ADC_FRAME_COUNT / channel.Divisor;
					}
					Assert.AreEqual (expectedLength, info.Length, "Wrong size for channel: " + name);
				}
			}
		}

		private void fillBuffer()
		{
			SdData data;
			for(uint i=0; i<Data.Constants.TOTAL_SECTORS; i++) 
			{
				data.adsRecordNumber = i;
			}
		}
	}
}

