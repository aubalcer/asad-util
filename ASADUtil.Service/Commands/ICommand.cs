﻿using System;

namespace ASADUtil.Service.Commands
{
    public interface ICommand
    {
        event EventHandler CanExecuteChanged;

        bool CanExecute(Object parameter);
        void Execute(Object parameter);
    }
}
