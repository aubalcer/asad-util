using System;
using Microsoft.Win32.SafeHandles;
using System.Collections.Generic;

namespace ASADUtil.Service.Disk
{
    public interface IDiskOperation
    {
		SafeFileHandle OpenFileForReading(String fileName);

		SafeFileHandle OpenAndLockDisk(String diskName, out ICollection<SafeFileHandle> mountedVolumes);
		void CloseAndUnlockDisk(SafeFileHandle handleValue, ICollection<SafeFileHandle> mountedVolumes);

		int ReadFile(SafeFileHandle handleValue, byte[] data, int read);
		int WriteFile(SafeFileHandle handleValue, byte[] data, int size);

		uint MovePointerToPos(SafeFileHandle handleValue, ulong pos);

    }
}

