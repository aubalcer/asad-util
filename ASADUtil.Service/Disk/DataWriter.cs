using ASADUtil.Service.Model;
using ASADUtil.Service.ChannelService;
using ASADUtil.Service.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ASADUtil.Service.Disk
{
    public class DataWriter
    {
        private string path;
        private Mera mera;

        private uint speedSensorTime;
        private float speed;

		private Dictionary<string, dynamic> channelWriters = new Dictionary<string, dynamic>();

        public DataWriter(string path, Mera mera)
        {
            this.path = path;
            this.mera = mera;
        }

        public void init()
        {
            IniFile meraIni = new IniFile(path + mera.FileName + ".mera");
            meraIni.IniSetValue("MERA", "Test", mera.Name);
            meraIni.IniSetValue("MERA", "Prod", mera.ProductName);
            meraIni.IniSetValue("MERA", "Date", mera.StartDate.ToString("dd.MM.yyyy"));

            channelWriters.Clear();
            speedSensorTime = 0;
            speed = 0;

            foreach (Channel ch in Mera.Channels)
            {
                for (int i = 0; i < ch.Total; i++)
                {
                    string name = string.Format(ch.Mask, ch.Name, i + 1);

					meraIni.IniSetValue(name, "Freq", (Record.SAMPLING_FREQUENCY/ch.Divisor).ToString());
                    meraIni.IniSetValue(name, "XUnits", ch.XUints);
                    meraIni.IniSetValue(name, "Comment", ch.Comment);
                    meraIni.IniSetValue(name, "YUnits", ch.YUnits);
                    meraIni.IniSetValue(name, "YFormat", ch.YFormat);
					//meraIni.IniSetValue(name, "XFormat", "R8");

                }

				dynamic channelWriter = null;
				switch(ch.YFormat) 
				{
					case "I4": channelWriter = new ChannelWriter<Int32>(this.path, ch.Mask, ch.Name, ch.Total, Record.SAMPLING_FREQUENCY/ch.Divisor); break;
					case "R4": channelWriter = new ChannelWriter<float>(this.path, ch.Mask, ch.Name, ch.Total, Record.SAMPLING_FREQUENCY/ch.Divisor); break;
				}
				channelWriters.Add(ch.Name, channelWriter);
            }

            meraIni.save();

        }

        unsafe public void writeFrequancy(UInt32 pulseTime, UInt32 currtime)
        {
            //if (speedSensorTime != 0 && pulseTime > speedSensorTime)
            //{
            //    if (speed == 0 || ((pulseTime - speedSensorTime) < 5000))
            //    {
            //        speed = (1000000 / (pulseTime - speedSensorTime));
            //    }

            //    speedSensorTime = pulseTime;
            //    watchTime = DateTime.Now.Ticks;
            //}
            //else if (speedSensorTime == 0)
            //{
            //    speedSensorTime = pulseTime;
            //}

            //if ((DateTime.Now.Ticks - watchTime) / 1000000 > 5)
            //{
            //    speed = 0;
            //}
            if ((currtime - pulseTime) > 2000000)
            {
                speed = 0;
            }

 //           if (speedSensorTime < pulseTime)
            {
                UInt32 period = pulseTime - speedSensorTime;
                if (period > 5000)
                {
                    if (period < 2000000)
                    {
                        speed = (float)(6.0e7 / (period));
                    } else
                    {
                        speed = 0;
                    }
                    speedSensorTime = pulseTime;
                }

 //               speed = (1000000 / (pulseTime - speedSensorTime));
                
//                watchTime = DateTime.Now.Ticks;
            }

/*
            if (DateTime.Now.Ticks - watchTime > 1000000 * 5)
            {
                speed = 0;
            }
*/

/*
           ushort imp1;
            if (speedSensorTime != pulseTime) { imp1 = 1000; } else {imp1 = 0;};
            speedSensorTime = pulseTime;

            channelWriters["tms_Simp"][0].Write(speed);

*/
			this.channelWriters["tms_S"].Write(speed, currtime);
        }

		unsafe public void write(SdData sdData, int index, StatusWord sw)
        {
			Int16 uAdc;
			int offset = index * SdData.ADC_FRAME_SIZE;
			UInt32 time = (UInt32) (index * 1e+6f / Record.SAMPLING_FREQUENCY) + sdData.TIME_ADS;

			List<float> tms_V = new List<float>();
			Int16[] factor = {1, -1, -1, 1, 1, -1, -1, 1};

            for (int i = 0; i < Mera.CHANNEL_COUNT; i++)
            {
				uAdc = (Int16)((byte)sdData.adcData[offset + 1] + (UInt16) (sdData.adcData[offset] << 8));
				tms_V.Add(factor[i] * sw.getVsg(uAdc));
	            offset += 2;
            }
			this.channelWriters["tms_V"].Write(tms_V, time);
       }


        public void close()
        {
			foreach (var writer in channelWriters.Values)
            {
				writer.Close();
            }
        }


		public void writeStatusWordData(StatusWord sw, uint time, long pos)
        {
			if (sw.Order == 0)
            {
				this.channelWriters["T(cpu)"].Write((Int32)(sw.Tcpu > 97 ? sw.Tcpu - 128 : sw.Tcpu), time);
            }
			else if (sw.Order == 2)
            {
				channelWriters["tms_Q"].Write(sw.Vads, time);
				channelWriters["tms_R"].Write(sw.RSg, time);
            }
			else if (sw.Order == 3)
            {
				channelWriters["Vsup"].Write(sw.Vsup, time);
				channelWriters["V(ref_meas)"].Write(sw.VRefMeas, time);
				channelWriters["PowerGood"].Write(sw.PowerGood, time);
            }
        }

		public void writeBreakBlock(uint time)
		{
			foreach (var writer in channelWriters.Values)
			{
				writer.WriteBreak(time);
			}
		}
    }
}
