using ASADUtil.Service.Model;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Linq;

namespace ASADUtil.Service.Disk
{
    public class ReaderEventArgs : EventArgs
    {
        public object Value { get; set; }

        public ReaderEventArgs()
        {
            this.Value = null;
        }

        public ReaderEventArgs(object value)
        {
            this.Value = value;
        }
    }

    public class DataReader
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(DataReader));
        private static log4net.ILog analizeLog = log4net.LogManager.GetLogger("Analize");

        private const int ADC_CHANNEL_COUNT = 8;
        private const int STATUS_WORD_FREQUENCY = 4;

		private IDiskOperation diskOperation;

        private Thread parseThread;
        private Thread cleanThread;
        private Thread anilizeThread;
        private Thread saveThread;
        private ManualResetEvent cancelEvent;

        public event EventHandler<ReaderEventArgs> ChangeProgressEvent;
        public event EventHandler<ReaderEventArgs> ErrorEvent;
        public event EventHandler<ReaderEventArgs> FinishEvent;
        public event EventHandler<ReaderEventArgs> FinishAnylizeEvent;
        public event EventHandler<ReaderEventArgs> FinishClearEvent;
        public event EventHandler<ReaderEventArgs> FindLengthEvent;

		public delegate void ReadOperation(SdData sdData, bool isDataWrong, ulong sector);
		public delegate void BaseOperation(SafeFileHandle handleValue, ulong pos);

		byte[] readBuffer = new byte[UsbDevice.IO_BUFFER_SIZE];


		public DataReader(IDiskOperation diskOperation)
		{
			this.cancelEvent = new ManualResetEvent(true);
			this.diskOperation = diskOperation;
		}

		public DataReader() : this(new DiskOperation())
        {
        }

		public void setDiskOperation(IDiskOperation diskOperation)
		{
			this.diskOperation = diskOperation;
		}

        public void stop()
        {
            cancelEvent.Reset();
        }

		public void convertAsync(UsbDevice sdCard, Mera meraInfo, string dstPath)
        {
            if (parseThread != null && parseThread.IsAlive)
            {
                return;
            }

            cancelEvent.Set();

            parseThread = new Thread(unused => convertProc(sdCard, meraInfo, dstPath));
            parseThread.Start();
        }

		public void convert(UsbDevice sdCard, Mera meraInfo, string dstPath)
		{
			cancelEvent.Set();
			convertProc(sdCard, meraInfo, dstPath);
		}

        public void anilizeAsync(UsbDevice sdCard)
        {
            if (anilizeThread != null && anilizeThread.IsAlive)
            {
                return;
            }

            cancelEvent.Set();

			anilizeThread = new Thread(unused => anilizeProc(sdCard));
            anilizeThread.Start();
        }

		public void anilize(UsbDevice sdCard)
		{
			cancelEvent.Set();
			anilizeProc(sdCard);
		}

        public void cleanAsync(UsbDevice sdCard)
        {
            if (cleanThread != null && cleanThread.IsAlive)
            {
                return;
            }

            cancelEvent.Set();

			cleanThread = new Thread(unused => cleanProc(sdCard));
            cleanThread.Start();
        }

		public void clean(UsbDevice sdCard)
		{
			cancelEvent.Set();
			cleanProc(sdCard);
		}

        public void saveAsync(UsbDevice SdCard, string fileName)
        {
            if (saveThread != null && saveThread.IsAlive)
            {
                return;
            }

            cancelEvent.Set();

            saveThread = new Thread(unused => saveProc(SdCard, fileName));
            saveThread.Start();
        }

		public void save(UsbDevice SdCard, string fileName)
		{
			cancelEvent.Set();
			saveProc(SdCard, fileName);
		}


        private void saveProc(UsbDevice sdCard, string fileName)
        {
			if (!System.IO.Path.IsPathRooted(fileName))
			{
				string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
				fileName = System.IO.Path.GetDirectoryName(path) + "/" + fileName;
			}

			string root = System.IO.Path.GetPathRoot(fileName);
			DriveInfo drive = new DriveInfo(root);
			if (drive.AvailableFreeSpace < (long)(sdCard.DiskRange.GetSize() * sdCard.BytesPerSector))
			{
				OnEvent(ErrorEvent, new Exception("Не достаточно места на диске"));
				return;
			}

			int size = (int)(sdCard.DiskRange.GetSize() * sdCard.BytesPerSector); 
			int read = 0;

			FileStream fs = null;

			try {
				fs = File.OpenWrite(fileName);

				generalDiskIteration(sdCard, false, (SafeFileHandle handleValue, ulong pos) =>
				{
					read = Math.Min(readBuffer.Length, size - (int)((pos - sdCard.DiskRange.StartPos) * sdCard.BytesPerSector));
					read = diskOperation.ReadFile(handleValue, readBuffer, read);
					if (read > 0)
					{
						fs.Write(readBuffer, 0, read);
					}

				});
			}  
			catch (Exception e)
			{
				OnEvent(ErrorEvent, e);
			}
			finally
			{
				if (fs != null)
				{
					fs.Close();
				}
			}

            OnEvent(FinishClearEvent, null);
        }

		private void cleanProc(UsbDevice sdCard)
        {
			int totalErrors = 0;

			int write = 0;
			ulong totalWrite = 0;
			int size = (int)(sdCard.DiskRange.GetSize() * sdCard.BytesPerSector); 

			readBuffer = readBuffer.Select(i => UsbDevice.EMPTY_BYTE).ToArray();

			try
			{
				generalDiskIteration(sdCard, true, (SafeFileHandle handleValue, ulong pos) => 
				{
					write = Math.Min(readBuffer.Length, size - (int)(pos * sdCard.BytesPerSector));
					write = diskOperation.WriteFile(handleValue, readBuffer, write);
					if (write < 0)
					{
						totalErrors++;
					}
					else 
					{
						totalWrite += (ulong)write;
					}

				});
			} 
			catch(Exception e)
			{
				OnEvent(ErrorEvent, e);
			}

            OnEvent(FinishClearEvent, null);
        }

        private void anilizeProc(UsbDevice sdCard)
        {
			Record rec = new Record();

			UInt32 sdTEMPold = 0;

			try 
			{
				readDiskIteration(sdCard, (SdData sdData, bool isDataWrong, ulong sec)=>
				{
					if (isDataWrong)
					{
						rec.CrcErrorCount++;

						string s = "crc er " + sec.ToString();
						analizeLog.Info(s);
					}
					else
					{
						if (sdData.adsRecordNumber != (sdTEMPold + 1))
						{
							ulong d = sdData.adsRecordNumber - sdTEMPold;
							string s1 = "break " + sec.ToString() + " " + d.ToString();
							analizeLog.Info(s1);
							rec.BreakCount++;
						}
					}
                    rec.Length++;
					sdTEMPold = sdData.adsRecordNumber;
				});
			} 
			catch(Exception e)
			{
				OnEvent(ErrorEvent, e);
			}

            OnEvent(FinishAnylizeEvent, rec);
        }

        private void convertProc(UsbDevice sdCard, Mera mera, string path)
        {
			DataWriter writer = new DataWriter(path, mera);
            writer.init();

			try 
			{
				long pos = 0;
				Int32 sdTEMPold = -1;

				readDiskIteration(sdCard, (SdData sdData, bool isDataWrong, ulong sector)=>
				{
	                if (!isDataWrong)
	                {
						if (sdData.adsRecordNumber != (sdTEMPold + 1))
						{
							writer.writeBreakBlock(sdData.TIME_ADS);
						}

						StatusWord sw  = readStatusWord(sdData);
						
	                    writer.writeFrequancy(sdData.TIME_DO, sdData.TIME_ADS);
						writer.writeStatusWordData(sw, sdData.TIME_ADS, pos);
	                    storeData(sdData, writer, sw);

						pos++;
					} 
					else 
					{
						writer.writeBreakBlock(sdData.TIME_ADS);
					}

					sdTEMPold = (Int32)sdData.adsRecordNumber;
				});

			} 
			catch(Exception e)
			{
				OnEvent(ErrorEvent, e);
			}
			writer.close(); 

			OnEvent(FinishEvent, null);
        }

		public unsafe StatusWord readStatusWord(SdData sdData) 
		{
			StatusWord sw = new StatusWord();

			sw.Order = (byte)(sdData.adsRecordNumber & 0x3);
			if (sw.Order == 0)
			{
				//PowerGood = ((byte)(sdData.statusWord[0] & 0xff)) == 4 ? 1 : 0;
				sw.Tcpu = sdData.statusWord.Tcpu;

				sw.Vtd = sdData.statusWord.Vtd;
				sw.Vrefm = sdData.statusWord.Vrefm;
				sw.Rb = sdData.statusWord.Rb;
				sw.Ku = sdData.statusWord.Ku;
			}
			else if (sw.Order == 2)
			{
				sw.Vads.Clear();
				float Vads;
				for (int i = 0; i < ADC_CHANNEL_COUNT; i++)
				{
					Vads = (float)(sdData.statusWord.Vads[i] & 0x7FFFFFFF) / 1000000;
					sw.Vads.Add(Vads);
					sw.RSg.Add(sw.getRSg(Vads));
				}
			}
			else if (sw.Order == 3)
			{
				sw.Vsup = (float)(sdData.statusWord.Vsup & 0x7FFFFFFF) / 1000000;
				sw.VRefMeas = (float)(sdData.statusWord.VRefMeas & 0x7FFFFFFF) / 1000000;

				sw.PowerGood = (Int32) (sw.Vsup > 5.9 ? 1 : 0);
			}

			return sw;
		}

		private unsafe void readDiskIteration(UsbDevice sdCard,  ReadOperation operation)
		{
			bool isDataWrong;
			int readBytes;

			int size = Marshal.SizeOf(typeof(SdData));
			int totalSize = (int)(sdCard.DiskRange.GetSize() * sdCard.BytesPerSector);

			generalDiskIteration(sdCard, false, (SafeFileHandle handleValue, ulong pos) =>
			{
				readBytes = Math.Min(UsbDevice.IO_BUFFER_SIZE, totalSize - (int)((pos - sdCard.DiskRange.StartPos) * sdCard.BytesPerSector));
				readBytes = diskOperation.ReadFile(handleValue, readBuffer, readBytes);

                if(readBuffer.Take((int) Math.Min(sdCard.BytesPerSector, readBytes)).All(val => val == UsbDevice.EMPTY_BYTE))
                {
                    return;
                }

				SdData sdData;
				int dataCount = readBytes / size;
				for (int j = 0; j < dataCount; j++)
				{
					sdData = readStruct<SdData>(readBuffer, j * size);
					byte[] crcData = getCrc(sdData);
					isDataWrong = sdData.crcByte[0] != crcData[0] || sdData.crcByte[1] != crcData[1];

					operation(sdData, isDataWrong, (ulong)j + pos);
				}
			});
		}

		private unsafe void generalDiskIteration(UsbDevice sdCard, bool lockDevice, BaseOperation operation)
		{
			SafeFileHandle handleValue = null;
			ICollection<SafeFileHandle> mountedVolumes = null;

			try
			{
				if(lockDevice) 
				{
					handleValue = diskOperation.OpenAndLockDisk(sdCard.DeviceId, out mountedVolumes);
				}
				else 
				{
					handleValue = diskOperation.OpenFileForReading(sdCard.DeviceId);
				}

				if (handleValue.IsInvalid)
				{
					Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
				}

				ulong step = (ulong)(UsbDevice.IO_BUFFER_SIZE / sdCard.BytesPerSector);

				diskOperation.MovePointerToPos(handleValue, sdCard.DiskRange.StartPos * sdCard.BytesPerSector);

				for (ulong i = sdCard.DiskRange.StartPos; i < sdCard.DiskRange.EndPos; i += step)
				{
					OnEvent(ChangeProgressEvent, i);

					operation(handleValue, i);

					if (!cancelEvent.WaitOne(0))
					{
						break;
					}
				}
			}
			catch (Exception e)
			{
				log.Error(e);
				throw e;
			}
			finally
			{
				if (handleValue != null)
				{
					if (lockDevice)
					{
						diskOperation.CloseAndUnlockDisk(handleValue, mountedVolumes);
					}
					else
					{
						handleValue.Close();
					}
				}
			}
		}

		unsafe private byte[] getCrc(SdData sdData)
		{
			CRC16 crc16 = new CRC16();
			byte[] crcData = crc16.ComputeHash((byte*)(&sdData.sdRecordType), 510);
			return crcData;
		}

        private unsafe void storeData(SdData sdData, DataWriter writer, StatusWord sw)
        {
			for (int k = 0; k < SdData.ADC_FRAME_COUNT; k++)
            {
				writer.write(sdData, k, sw);
            }
        }

        private T readStruct<T>(byte[] data, int start)
        {
            int size = Marshal.SizeOf(typeof(T));
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(data, start, ptr, size);
            T t = (T)Marshal.PtrToStructure(ptr, typeof(T));
            Marshal.FreeHGlobal(ptr);

            return t;
        }

        private void OnEvent(EventHandler<ReaderEventArgs> e, object param)
        {
            if (e != null)
            {
                e(this, new ReaderEventArgs(param));
            }
        }
    }
}
