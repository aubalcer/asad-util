﻿using ASADUtil.Service.Model;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;

namespace ASADUtil.Service.Disk
{
	public class DiskOperation: IDiskOperation
    {
        #region "API CALLS"
        public static short FILE_ATTRIBUTE_NORMAL = 0x80;
        public static short INVALID_HANDLE_VALUE = -1;
        public static uint GENERIC_READ = 0x80000000;
        public static uint GENERIC_WRITE = 0x40000000;
        public static uint CREATE_NEW = 1;
        public static uint CREATE_ALWAYS = 2;
        public static uint OPEN_EXISTING = 3;
        public static uint FILE_SHARE_READ = 0x00000001;
        public static uint FILE_SHARE_WRITE = 0x00000002;

        public const int IOCTL_DISK_GET_LENGTH_INFO = 0x0007405c;
        public const int IOCTL_DISK_GET_DRIVE_GEOMETRY = 0x00070000;

        public const int FSCTL_LOCK_VOLUME = 0x00090018;
        public const int FSCTL_DISMOUNT_VOLUME = 0x00090020;
        public const int FSCTL_UNLOCK_VOLUME = 0x0009001c;

        public const uint FILE_FLAG_NO_BUFFERING = 0x20000000;
        public const uint FILE_FLAG_RANDOM_ACCESS = 0x10000000;
        public const uint FILE_FLAG_WRITE_THROUGH = 0x80000000;

        const uint FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
        const uint FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
        const uint FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;
        const uint FORMAT_MESSAGE_ARGUMENT_ARRAY = 0x00002000;
        const uint FORMAT_MESSAGE_FROM_HMODULE = 0x00000800;
        const uint FORMAT_MESSAGE_FROM_STRING = 0x00000400;

        public enum EMoveMethod : uint
        {
            Begin = 0,
            Current = 1,
            End = 2
        }

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetVolumePathNamesForVolumeNameW(string lpszVolumeName,
                [Out] StringBuilder lpszVolumePathNames, uint cchBuferLength,
                out UInt32 lpcchReturnLength);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr FindFirstVolume([Out] StringBuilder lpszVolumeName,
           uint cchBufferLength);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool FindNextVolume(IntPtr hFindVolume, [Out] StringBuilder lpszVolumeName, uint cchBufferLength);

        [DllImport("kernel32.dll", ExactSpelling = true, SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool DeviceIoControl(SafeFileHandle hDevice, uint dwIoControlCode,
            IntPtr lpInBuffer, uint nInBufferSize,
            IntPtr lpOutBuffer, uint nOutBufferSize,
            out uint lpBytesReturned, IntPtr lpOverlapped);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool FindVolumeClose(IntPtr hFindVolume);

        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern uint SetFilePointer(
            [In] SafeFileHandle hFile,
            [In] int lDistanceToMove,
            [Out] out int lpDistanceToMoveHigh,
            [In] EMoveMethod dwMoveMethod);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern SafeFileHandle CreateFile(string lpFileName, uint dwDesiredAccess,
          uint dwShareMode, IntPtr lpSecurityAttributes, uint dwCreationDisposition,
          uint dwFlagsAndAttributes, IntPtr hTemplateFile);

		[DllImport("kernel32.dll", SetLastError = true)]
        public extern static int ReadFile(SafeFileHandle handle, byte[] bytes,
           int numBytesToRead, out int numBytesRead, IntPtr overlapped_MustBeZero);

		[DllImport("kernel32.dll", SetLastError = true)]
        public extern static int WriteFile(SafeFileHandle handle, byte[] bytes,
           int numBytesToWrite, out int numBytesWriten, IntPtr overlapped_MustBeZero);

		[DllImport("kernel32.dll", SetLastError = true)]
        public extern static uint GetLastError();

        [DllImport("kernel32.dll")]
        public static extern uint FormatMessage(uint dwFlags, IntPtr lpSource,
           uint dwMessageId, uint dwLanguageId, [Out] StringBuilder lpBuffer,
           uint nSize, IntPtr Arguments);

        public enum MEDIA_TYPE : uint
        {
            Unknown,
            F5_1Pt2_512,
            F3_1Pt44_512,
            F3_2Pt88_512,
            F3_20Pt8_512,
            F3_720_512,
            F5_360_512,
            F5_320_512,
            F5_320_1024,
            F5_180_512,
            F5_160_512,
            RemovableMedia,
            FixedMedia,
            F3_120M_512,
            F3_640_512,
            F5_640_512,
            F5_720_512,
            F3_1Pt2_512,
            F3_1Pt23_1024,
            F5_1Pt23_1024,
            F3_128Mb_512,
            F3_230Mb_512,
            F8_256_128,
            F3_200Mb_512,
            F3_240M_512,
            F3_32M_512
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DISK_GEOMETRY
        {
            public long Cylinders;
            public MEDIA_TYPE MediaType;
            public int TracksPerCylinder;
            public int SectorsPerTrack;
            public int BytesPerSector;

            public long DiskSize
            {
                get
                {
                    return Cylinders * (long)TracksPerCylinder * (long)SectorsPerTrack * (long)BytesPerSector;
                }
            }

            public long TotalSectors
            {
                get
                {
                    return Cylinders * (long)TracksPerCylinder * (long)SectorsPerTrack;
                }
            }
        }

        #endregion

        #region "WMI LOW LEVEL COMMANDS"

        /// <summary>
        /// Returns the number of bytes that the drive sectors contain.
        /// </summary>
        /// <param name="drive">
        /// Int: The drive number to scan.
        /// </param>
        /// <returns>
        /// Int: The number of bytes the sector contains.
        /// </returns>
        public static int BytesPerSector(int drive)
        {
            int driveCounter = 0;
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_DiskDrive");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (driveCounter == drive)
                    {
                        var t = queryObj["BytesPerSector"];
                        return int.Parse(t.ToString());

                    }
                    driveCounter++;
                }
            }
            catch (ManagementException)
            {
                return -1;
            }
            return 0;
        }

        public static ulong RecordSize(SafeFileHandle handleValue, UsbDevice device)
        {
            ulong totalSectors = device.TotalSectors;
            uint size = device.BytesPerSector;
            ulong min = 0;
            ulong max = totalSectors;
            ulong sector;
            bool res = false;

            if (CheckSector(handleValue, 0, size))
            {
                return 0;
            }

            if (!CheckSector(handleValue, totalSectors - 1, size))
            {
                return totalSectors;
            }


            while (Math.Abs((float)(max - min)) > 1)
            {
                sector = (max + min) / 2;
                res = CheckSector(handleValue, sector, size);
                if (res)
                {
                    max = sector;
                }
                else
                {
                    min = sector;
                }

            }

            if (res)
            {
                sector = min + 1;
            }
            else
            {
                sector = max;
            }

            return sector;
        }

        public static bool CheckSector(SafeFileHandle hFile, ulong sector, uint bytesPerSector)
        {
            byte[] buf = new byte[bytesPerSector];
            int read = 0;
            Int64 sec = (Int64)(sector * bytesPerSector);
            int trx = (int)(sec & 0xffffffff);
            int moveToHigh = (int)((sec >> 32) & 0xffffffff);

            SetFilePointer(hFile, trx, out moveToHigh, EMoveMethod.Begin);
            ReadFile(hFile, buf, (int)bytesPerSector, out read, IntPtr.Zero);

            for (int i = 0; i < bytesPerSector; i++)
            {
                if (buf[i] != 255) return false;
            }

            return true;
        }

        /// <summary>
        /// Returns the number of bytes that the drive sectors contain.
        /// </summary>
        /// <param name="drive">
        /// Int: The drive number to scan.
        /// </param>
        /// <returns>
        /// Int: The number of bytes the sector contains.
        /// </returns>
        public static int BytesPerSector(string deviceID)
        {
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_DiskDrive WHERE DeviceID = '" + deviceID.Replace("\\", "\\\\") + "'");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    var t = queryObj["BytesPerSector"];
                    return (t != null) ? int.Parse(t.ToString()) : -1;
                }
            }
            catch (ManagementException)
            {
                return -1;
            }
            return 0;
        }

        /// <summary>
        /// Returns a list of physical drive IDs
        /// </summary>
        /// <returns>
        /// ArrayList: Device IDs of all connected physical hard drives
        ///  </returns>
        public static IEnumerable<UsbDevice> GetDriveList()
        {
            List<UsbDevice> usblist = new List<UsbDevice>();

            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM  Win32_DiskDrive WHERE InterfaceType='USB' AND TotalSectors > 0");

                UsbDevice device;
                foreach (ManagementObject queryObj in searcher.Get())
                {
                    device = UsbDevice.fromQueryObj(queryObj);
                    SafeFileHandle hFile = CreateFile(device.DeviceId, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                    if (!hFile.IsInvalid)
                    {
                        IntPtr buffer = Marshal.AllocHGlobal(8);
                        uint bytesReturned;
                        bool res = DeviceIoControl(hFile, IOCTL_DISK_GET_LENGTH_INFO, IntPtr.Zero, 0, buffer, 8, out bytesReturned, IntPtr.Zero);
                        if (res)
                        {
                            device.Size = (ulong)Marshal.ReadInt64(buffer);
                            device.TotalSectors = device.Size / device.BytesPerSector;
                        }

                        Marshal.FreeHGlobal(buffer);
                        hFile.Close();
                    }
                    usblist.Add(device);
                }
            }
            catch (ManagementException)
            {
                return null;
            }
            return usblist;
        }

        /// <summary>
        /// Returns the total sectors on the specified drive
        /// </summary>
        /// <param name="drive">
        /// int: The drive to be queried.
        /// </param>
        /// <returns>
        /// int: Returns the total number of sectors
        /// </returns>
        public static int GetTotalSectors(int drive)
        {
            int driveCount = 0;
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_DiskDrive");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (driveCount == drive)
                    {
                        var t = queryObj["TotalSectors"];
                        return int.Parse(t.ToString());

                    }
                    driveCount++;
                }
            }
            catch (ManagementException)
            {
                return -1;
            }
            return -1;
        }

        public static long GetSize(string deviceID)
        {
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_DiskDrive WHERE DeviceID = '" + deviceID.Replace("\\", "\\\\") + "'");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    var t = queryObj["Size"];
                    return (t != null) ? long.Parse(t.ToString()) : -1;
                }
            }
            catch (ManagementException)
            {
                return -1;
            }
            return -1;
        }

        /// <summary>
        /// Returns the total sectors on the specified drive
        /// </summary>
        /// <param name="drive">
        /// int: The drive to be queried.
        /// </param>
        /// <returns>
        /// int: Returns the total number of sectors
        /// </returns>
        public static int GetTotalSectors(string deviceID)
        {
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_DiskDrive WHERE DeviceID = '" + deviceID.Replace("\\", "\\\\") + "'");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    var t = queryObj["TotalSectors"];
                    return (t != null) ? int.Parse(t.ToString()) : -1;
                }
            }
            catch (ManagementException)
            {
                return -1;
            }
            return -1;
        }

        /// <summary>
        /// Returns the number of Sectors per track.
        /// </summary>
        /// <param name="drive">
        /// The drive to be queried.
        /// </param>
        /// <returns>
        /// int: the number of sectors per track
        /// </returns>
        public static int GetSectorsPerTrack(int drive)
        {
            int driveCount = 0;
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_DiskDrive");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (driveCount == drive)
                    {
                        var t = queryObj["SectorsPerTrack"];
                        return (t != null) ? int.Parse(t.ToString()) : -1;
                    }
                    driveCount++;
                }
            }
            catch (ManagementException)
            {
                return -1;
            }
            return -1;
        }

        /// <summary>
        /// Returns the total number of tracks on this hard drive
        /// </summary>
        /// <param name="drive">
        /// The drive to be parsed
        /// </param>
        /// <returns>
        /// int: the number of tracks on the drive
        /// </returns>
        public static int GetTotalTracks(int drive)
        {
            int DriveCount = 0;
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_DiskDrive");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (DriveCount == drive)
                    {
                        var t = queryObj["TotalTracks"];
                        return (t != null) ? int.Parse(t.ToString()) : -1;
                    }
                    DriveCount++;
                }

            }
            catch (ManagementException)
            {
                return -1;
            }
            return -1;
        }

        #endregion

        public static string GetLasErrorMessage()
        {
            StringBuilder strBuilder = new StringBuilder(255);
            uint errorCode = GetLastError();
            FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, IntPtr.Zero, errorCode, 0U, strBuilder, 255U, IntPtr.Zero);

            return strBuilder.ToString();
        }

        private string md5Hash(string p)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(p);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();

        }

        public SafeFileHandle OpenAndLockDisk(string deviceID, out ICollection<SafeFileHandle> mountedVolumes)
        {
            ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    @"SELECT * FROM Win32_DiskDrive WHERE DeviceId='" + deviceID.Replace("\\", "\\\\") + "'");

            SafeFileHandle hDiskFile = null;
            uint size = 0;

            mountedVolumes = new List<SafeFileHandle>();
            foreach (ManagementObject mo in searcher.Get())
            {
                foreach (ManagementObject b in mo.GetRelated("Win32_DiskPartition"))
                {
                    foreach (ManagementBaseObject c in b.GetRelated("Win32_LogicalDisk"))
                    {
                        SafeFileHandle hFile = CreateFile(string.Format("\\\\.\\{0}", c["Name"]),
                            GENERIC_READ,
                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                            IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);

                        if (!hFile.IsInvalid)
                        {
                            mountedVolumes.Add(hFile);

                            DeviceIoControl(hFile, FSCTL_LOCK_VOLUME, IntPtr.Zero, 0U, IntPtr.Zero, 0U, out size, IntPtr.Zero);
                            DeviceIoControl(hFile, FSCTL_DISMOUNT_VOLUME, IntPtr.Zero, 0U, IntPtr.Zero, 0U, out size, IntPtr.Zero);
                        }
                    }
                }
            }

            hDiskFile = CreateFile(deviceID,
                GENERIC_READ | GENERIC_WRITE,
                0,
                IntPtr.Zero, OPEN_EXISTING,
                FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH, IntPtr.Zero);
            
            if (!hDiskFile.IsInvalid)
            {
                DeviceIoControl(hDiskFile, FSCTL_LOCK_VOLUME, IntPtr.Zero, 0U, IntPtr.Zero, 0U, out size, IntPtr.Zero);
                DeviceIoControl(hDiskFile, FSCTL_DISMOUNT_VOLUME, IntPtr.Zero, 0U, IntPtr.Zero, 0U, out size, IntPtr.Zero);
            }

            return hDiskFile;
        }

		public void CloseAndUnlockDisk(SafeFileHandle hDiskFile, ICollection<SafeFileHandle> mountedVolumes)
        {
            uint size;
            if (hDiskFile != null)
            {
                DeviceIoControl(hDiskFile, FSCTL_UNLOCK_VOLUME, IntPtr.Zero, 0U, IntPtr.Zero, 0U, out size, IntPtr.Zero);
                hDiskFile.Close();
            }

            foreach (SafeFileHandle hFile in mountedVolumes)
            {
                DeviceIoControl(hFile, FSCTL_UNLOCK_VOLUME, IntPtr.Zero, 0U, IntPtr.Zero, 0U, out size, IntPtr.Zero);
                hFile.Close();
            }
        }

        /// <summary>
        /// Returns the Sector from the drive at the specified location
        /// </summary>
        /// <param name="drive">
        /// The drive to have a sector read
        /// </param>
        /// <param name="sector">
        /// The sector number to read.
        /// </param>
        /// <param name="bytesPerSector"></param>
        /// <returns></returns>
        public static byte[] DumpSector(string drive, double sector, int bytesPerSector)
        {
            SafeFileHandle handleValue = CreateFile(drive, GENERIC_READ, 0, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
            if (handleValue.IsInvalid)
            {
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }
            Int64 sec = (Int64)(sector * bytesPerSector);
            int size = (int)(sec & 0xffffffff);
            int moveToHigh = (int)((sec >> 32) & 0xffffffff);

            byte[] buf = new byte[bytesPerSector];
            int read = 0;
            SetFilePointer(handleValue, size, out moveToHigh, EMoveMethod.Begin);
            ReadFile(handleValue, buf, bytesPerSector, out read, IntPtr.Zero);
            handleValue.Close();
            return buf;
        }

        public static int WriteSector(string drive, double sector, int bytesPerSector, byte[] data)
        {
            SafeFileHandle handleValue = CreateFile(drive, GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
            if (handleValue.IsInvalid)
            {
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }

            Int64 sec = (Int64)(sector * bytesPerSector);
            int size = (int)(sec & 0xffffffff);
            int moveToHigh = (int)((sec >> 32) & 0xffffffff);
            int write;

            SetFilePointer(handleValue, size, out moveToHigh, EMoveMethod.Begin);
            WriteFile(handleValue, data, bytesPerSector, out write, IntPtr.Zero);
            handleValue.Close();
            return write;
        }

        /// <summary>
        /// Returns the Sector from the drive at the specified location
        /// </summary>
        /// <param name="drive">
        /// The drive to have a sector read
        /// </param>
        /// <param name="sector">
        /// The sector number to read.
        /// </param>
        /// <param name="bytesPerSector"></param>
        /// <returns></returns>
        private byte[] DumpTrack(string drive, double track, int bytesPerTrack, int TrackBufferSize)
        {
            SafeFileHandle handleValue = CreateFile(drive, GENERIC_READ, 0, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
            if (handleValue.IsInvalid)
            {
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }
            int size = int.Parse(bytesPerTrack.ToString());
            byte[] buf = new byte[size * TrackBufferSize];
            int read = 0;

            Int64 sec = (Int64)(track * bytesPerTrack * TrackBufferSize);
            int trx = (int)(sec & 0xffffffff);
            int moveToHigh = (int)((sec >> 32) & 0xffffffff);

            SetFilePointer(handleValue, trx, out moveToHigh, EMoveMethod.Begin);
            ReadFile(handleValue, buf, size, out read, IntPtr.Zero);
            handleValue.Close();
            return buf;
        }

		public static ulong RecordSize(UsbDevice sdCard)
        {
            if (sdCard.DeviceId == null)
            {
                return 0;
            }

            SafeFileHandle handleValue = CreateFile(sdCard.DeviceId, GENERIC_READ, 0, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
            if (handleValue.IsInvalid)
            {
                return 0;
            }

            ulong size = RecordSize(handleValue, sdCard);
            handleValue.Close();

            return size;

        }


		public SafeFileHandle OpenFileForReading(String fileName) 
		{
			return CreateFile(fileName, DiskOperation.GENERIC_READ, 0, IntPtr.Zero, DiskOperation.OPEN_EXISTING, 0, IntPtr.Zero);
		}


		public int ReadFile(SafeFileHandle handleValue, byte[] data, int read) 
		{
			int totalReadBytes;
			DiskOperation.ReadFile(handleValue, data, read, out totalReadBytes, IntPtr.Zero);

			return totalReadBytes;
		}

		public int WriteFile(SafeFileHandle handleValue, byte[] data, int size) 
		{
			int totalWrite = 0;
			int res = WriteFile(handleValue, data, size, out totalWrite, IntPtr.Zero);
			return (res == 0) ? -1 : totalWrite;
		}

		public uint MovePointerToPos(SafeFileHandle handleValue, ulong pos)
		{
			int trx = (int)(pos & 0xffffffff);
			int moveToHigh = (int)((pos >> 32) & 0xffffffff);

			return DiskOperation.SetFilePointer(handleValue, trx, out moveToHigh, DiskOperation.EMoveMethod.Begin);
		}

    }
}