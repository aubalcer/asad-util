using ASADUtil.Service.Commands;
using ASADUtil.Service.Model;
using ASADUtil.Service.Utils;
using ASADUtil.Service.Disk;
using System.Collections.ObjectModel;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;


namespace ASADUtil.Service
{
    public class ViewModel : BaseViewModel
    {
        private DataReader reader;

        private UsbDevice sdCard = new UsbDevice();

        private bool isFromDisk = true;

        private ObservableCollection<UsbDevice> listDrive;
        private Mera meraInfo = new Mera();

        private bool isError = false;
        private string errorMessage;

        private string destinationPath;
        private string dumpSourceFileName;

        private bool isParsingStart = false;
        private bool canBeginParse = false;

        private bool isAllDiskArea = true;
        private bool isFillDiskArea = false;
        private bool isCustomDiskArea = false;

        public ObservableCollection<UsbDevice> ListDrive
        {
            get { return this.listDrive; }
            set { this.listDrive = value; RaisePropertyChanged("ListDrive"); }
        }

        public Mera MeraInfo
        {
            get
            {
                this.validate();
                return meraInfo;
            }
        }

        public UsbDevice SdCard
        {
            get { return this.sdCard; }
            set { this.sdCard = value; reinitCard();  RaisePropertyChanged("SdCard"); }
        }

        public bool IsFromDisk
        {
            set { this.isFromDisk = value; RaisePropertyChanged("IsFromDisk"); }
            get { return isFromDisk; }
        }


        public bool IsAllDiskArea
        {
            set { this.isAllDiskArea = value; RaisePropertyChanged("IsAllDiskArea"); }
            get { return isAllDiskArea; }
        }

        public bool IsFillDiskArea
        {
            set { this.isFillDiskArea = value; RaisePropertyChanged("IsFillDiskArea"); }
            get { return isFillDiskArea; }
        }

        public bool IsCustomDiskArea
        {
            set { this.isCustomDiskArea = value; RaisePropertyChanged("IsCustomDiskArea"); }
            get { return isCustomDiskArea; }
        }


        public bool IsError
        {
            set { this.isError = value; RaisePropertyChanged("IsError"); }
            get { return isError; }
        }


        public string ErrorMessage
        {
            get { return this.errorMessage; }
            set
            {
                this.errorMessage = value;
                RaisePropertyChanged("ErrorMessage");
            }
        }

        public string DumpSourceFileName
        {
            get { return this.dumpSourceFileName; }
            set
            {
                this.dumpSourceFileName = null;
                if (value == "" || value == null)
                {
                    throw new Exception("Имя файла не может быть пустым");
                }

                if (!File.Exists(value))
                {
                    throw new Exception("Указанного файла не существует");
                }

                this.dumpSourceFileName = value;
                using (FileStream fs = File.OpenRead(value))
                {
                    SdCard = UsbDevice.deserialize(fs, value);
                }
                reinitCard();

                RaisePropertyChanged("DumpSourceFileName");
            }
        }
        public string DestinationPath
        {
            get { return this.destinationPath; }
            set
            {
                this.destinationPath = null;
                canBeginParse = false;

                if (value == "" || value == null)
                {
                    throw new Exception("Путь не должен быть пустым");
                }

                if (!Directory.Exists(value))
                {
                    throw new Exception("Указанного пути не существует");
                }

                this.destinationPath = value;
                this.validate();
                RaisePropertyChanged("DestinationPath");
            }
        }

        public bool IsParsingStart
        {
            set { this.isParsingStart = value; RaisePropertyChanged("IsParsingStart"); }
            get { return isParsingStart; }
        }

        public bool CanBeginParse
        {
            get { return this.canBeginParse; }
        }

        public Progress ProcessProgress { get; private set; }

        public Command SaveDumpCommand { get; private set; }
        public Command ClearCommand { get; private set; }
        public Command CancelCommand { get; private set; }
        public Command AnalizeCommand { get; private set; }
        public Command DiskRangeChangeCommand { get; private set; }
        public Command RefreshDeviceCommand { get; private set; }
        public Command SaveCommand { get; private set; }



        public ViewModel()
        {
            ListDrive = new ObservableCollection<UsbDevice>();
            refreshDevices();

			reader = new DataReader();
            reader.ErrorEvent += reader_ErrorEvent;
            reader.FinishEvent += reader_FinishEvent;
            reader.ChangeProgressEvent += reader_ChangeProgressEvent;
            reader.FinishAnylizeEvent += reader_FinishAnylizeEvent;
            reader.FinishClearEvent += reader_FinishClearEvent;
            reader.FindLengthEvent += reader_FindLengthEvent;

            ProcessProgress = new Progress();

            SaveDumpCommand = new Command(saveDump);
            CancelCommand = new Command(cancel);
            ClearCommand = new Command(clear);
            AnalizeCommand = new Command(analize);
            DiskRangeChangeCommand = new Command(rangeChange);
            RefreshDeviceCommand = new Command(refreshDevices);
            SaveCommand = new Command(saveData);
        }

        private void saveData(object param)
        {
            IsParsingStart = true;
            reader.saveAsync(SdCard, (string)param);
            ProcessProgress.Start();
        }

        private void refreshDevices()
        {
            ListDrive.Clear();
            IEnumerable<UsbDevice> devices = DiskOperation.GetDriveList();
            ListDrive.Add(UsbDevice.EmptyDevice);
            foreach (UsbDevice device in devices)
            {
                ListDrive.Add(device);
            }

            sdCard = null;
        }

        void rangeChange(object param)
        {
            switch (param.ToString())
            {
                case "Y":
                    SdCard.setDefaultRange();
                    break;
                case "F":
                    SdCard.setFillAreaRange(DiskOperation.RecordSize(SdCard));
                    break;
                case "N":
                    break;
            }

            bool isAllDisk = "Y".SequenceEqual((string)param);
            if (isAllDisk)
            {
                SdCard.setDefaultRange();
            }
        }

        void reader_FindLengthEvent(object sender, ReaderEventArgs e)
        {
            SdCard.DataRecord.Length = ((Record)e.Value).Length;
        }

        void reader_FinishClearEvent(object sender, ReaderEventArgs e)
        {
            SdCard.DataRecord = new Record();
            IsParsingStart = false;
        }

        void reader_FinishAnylizeEvent(object sender, ReaderEventArgs e)
        {
            SdCard.DataRecord = (Record)e.Value;
            IsParsingStart = false;
        }

        void reader_FinishEvent(object sender, ReaderEventArgs e)
        {
            IsParsingStart = false;
        }

        void reader_ErrorEvent(object sender, ReaderEventArgs e)
        {
            showError(((Exception)e.Value).Message);
        }

        private void analize()
        {
            IsParsingStart = true;
            reader.anilizeAsync(SdCard);
            ProcessProgress.Start();
        }

        private void clear(object confirm)
        {
            if ("Y".Equals(confirm))
            {
                IsParsingStart = true;
                ProcessProgress.Start();
                reader.cleanAsync(SdCard);
            }
        }

        private void validate()
        {
            canBeginParse = meraInfo.validate() && destinationPath != null;
            RaisePropertyChanged("CanBeginParse");
        }

        private void showError(string p)
        {
            IsError = true;
            IsParsingStart = false;
            ErrorMessage = p;
        }

        private void cancel()
        {
            IsParsingStart = false;
            if (reader != null)
            {
                reader.stop();
            }
        }

        private void reinitCard()
        {
            if (SdCard != null)
            {
                SdCard.setDefaultRange();

                SdCard.DataRecord.BytesPerSector = SdCard.BytesPerSector;
                SdCard.DataRecord.Length = DiskOperation.RecordSize(SdCard);

                if (SdCard.DataRecord.Length > 0)
                {
                    SdCard.DiskRange.EndPos = SdCard.DataRecord.Length;
                    IsFillDiskArea = true;
                }
            }
        }

        private void saveDump()
        {
            IsParsingStart = true;
            string path = DestinationPath;
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            path += MeraInfo.FileName + "\\";

            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                ProcessProgress.Start();
                reader.convertAsync(SdCard, MeraInfo, path);
            }
            catch (Exception e)
            {
                this.reader_ErrorEvent(null, new ReaderEventArgs(e));
            }
        }

        void reader_ChangeProgressEvent(object sender, ReaderEventArgs e)
        {
            SdCard.DiskRange.CurrentPos = (ulong)e.Value;
            ProcessProgress.Update(SdCard.DiskRange);
        }

    }
}
