﻿using IniParser;
using System;
using System.IO;
using System.Text;

namespace ASADUtil.Service.Utils
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

		private IniData iniData;
		private FileIniDataParser parser;

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath"></PARAM>
        public IniFile(string INIPath)
        {
            path = INIPath;
			parser = new FileIniDataParser();
			iniData = new IniData();
        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void IniSetValue(string Section, string Key, string Value)
        {
			if (!iniData.Sections.ContainsSection(Section))
			{
				iniData.Sections.AddSection(Section);
			}

			if (!iniData[Section].ContainsKey(Key))
			{
				iniData[Section].AddKey(Key);
			}

            iniData[Section][Key] = Value;
        }
        
        public void save()
        {
            using (FileStream fs = File.Open(path, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sr = new StreamWriter(fs, Encoding.GetEncoding("windows-1251")))
                {
                    parser.WriteData(sr, iniData);
                }
            }
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Path"></PARAM>
        /// <returns></returns>
        public string IniReadValue(string Section, string Key)
        {
			if (!iniData.Sections.ContainsSection(Section))
			{
				return String.Empty;
			}

			if (!iniData[Section].ContainsKey(Key))
			{
				return String.Empty;
			}

			return iniData[Section][Key];
        }
    }
}
