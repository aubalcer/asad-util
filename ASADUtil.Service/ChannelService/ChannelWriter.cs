using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;

namespace ASADUtil.Service.ChannelService
{
	public class ChannelWriter<T>
    {
		public static float TICK_FREQ = 1e+6f;

		private BinaryWriter[] writers;
		private BinaryWriter[] timeSeriesWriter;
		private StreamWriter[] breakBlockWriter;
		private string channelName;
		private int recordCount = 0;
		private double freq;

		public ChannelWriter(String path, String mask, String name, int total, double freq)
        {
			this.writers = new BinaryWriter[total];
			//this.timeSeriesWriter = new BinaryWriter[total];
			this.breakBlockWriter = new StreamWriter[total];

			for (int i = 0; i < total; i++)
			{
				string filename = string.Format(mask, name, i + 1);
				this.writers[i] = new BinaryWriter(new FileStream(path + filename + ".dat", FileMode.OpenOrCreate));
				//this.timeSeriesWriter[i] = new BinaryWriter(new FileStream(path + filename + ".x", FileMode.OpenOrCreate));
				this.breakBlockWriter[i] = new StreamWriter(new FileStream(path + filename + ".prt", FileMode.OpenOrCreate));

			}

			this.freq = freq;

			channelName = name;
        }

		public void Write(T val, ulong time)
		{
			for (int i=0; i<this.writers.Length; i++)
			{
				this.WriteInternal(val, i, time);
			}
			recordCount++;
		}

		public void Write(List<T> val, ulong time)
		{
			for (int i=0; i<val.Count; i++)
			{
				this.WriteInternal(val[i], i, time);
			}
			recordCount++;
		}

		public void Close() 
		{
			foreach (BinaryWriter writer in this.writers)
			{
				writer.Close();
			}

//			foreach (BinaryWriter writer in this.timeSeriesWriter)
//			{
//				writer.Close();
//			}

			foreach (StreamWriter writer in this.breakBlockWriter)
			{
				writer.Close();
			}
		}

		public void WriteBreak(ulong time)
		{
			double timeSec = (double) recordCount / freq;

			for (int i=0; i<this.writers.Length; i++)
			{
				breakBlockWriter[i].Write("{0} {1}\r\n", timeSec, recordCount);
			}
		}

		private void WriteInternal(T val, int pos, ulong time) 
		{
			double timeSec = (double) time / TICK_FREQ;
			//this.timeSeriesWriter[pos].Write(timeSec);

			if(val.GetType() == typeof(float)) 
			{
				this.writers[pos].Write(float.Parse(val.ToString()));
			}
			else if(val.GetType() == typeof(Int32)) 
			{
				this.writers[pos].Write(Int32.Parse(val.ToString()));
			}
			else 
			{
				throw new Exception("Try to save unknown value type " + val.GetType() + " to channel " + channelName);
			}
		}
    }
}

