using System;
using System.Runtime.InteropServices;

namespace ASADUtil.Service.Model
{

	[StructLayout(LayoutKind.Explicit, Pack=1)]
	public unsafe struct StatusWordData {
		//status = 0
		[FieldOffset(0)]
		public byte PowerGood;

		[FieldOffset(1)]
		public byte Tcpu;

		[FieldOffset(2)]
		public Int16 Vtd;

		[FieldOffset(4)]
		public Int16 Vrefm;

		[FieldOffset(8)]
		public Int16 Rb;

		[FieldOffset(14)]
		public UInt16 Ku;

		//status = 1
		[FieldOffset(0)]
		public fixed UInt32 Vads[StatusWord.ADC_CHANNEL_COUNT];

		//status = 2
		[FieldOffset(0)]
		public UInt32 Vsup;

		[FieldOffset(4)]
		public UInt32 VRefMeas;

	}

	[StructLayout(LayoutKind.Sequential, Pack=1)]
	public unsafe struct SdData
	{
		public const int ADC_FRAME_COUNT = 27;
		public const int ADC_FRAME_SIZE = 16;
		private const int ADC_DATA_SIZE = ADC_FRAME_COUNT * ADC_FRAME_SIZE;
		private const int DEBUG_DATA_SIZE = 20;
		private const int SD_BUFFER_GAP = 512 - 4 - 4 - 4 - 4 - ADC_FRAME_COUNT * ADC_FRAME_SIZE - 4 - 4 - 20 - 8 * 4 - 2; // 2

		public UInt32 sdRecordType;     //0
		public UInt32 sdRecordNumber;   //4
		public UInt32 adsRecordNumber;  //8
		public UInt32 adsFrameNumber;   //12

		public fixed byte adcData[ADC_DATA_SIZE]; //16 16*27

		public UInt32 TIME_DO; 	//448
		public UInt32 TIME_ADS;	//452

		public fixed byte debugData[DEBUG_DATA_SIZE]; //456

		public StatusWordData statusWord;//476

		public fixed byte free[SD_BUFFER_GAP]; //508 size = 2
		public fixed byte crcByte[2]; //510
	}
}

