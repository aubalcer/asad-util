﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASADUtil.Service.Model
{
    public class Record : BaseModel
    {
        public static int SAMPLING_FREQUENCY = 10547;

        public struct StatusWord
        {
            public byte powerComparator;
            public byte temperature;
            public UInt16 current;
        }

        #region Private Properties

        private ulong length;
        private long breakCount;
        private long crcErrorCount;
        private ulong bytesSize;

        #endregion

        public Record()
        {
            BytesPerSector = 512;
        }

        #region Public Properties

        public ulong Length
        {
            set
            {
                this.length = value;
                this.bytesSize = value * BytesPerSector;

                RaisePropertyChanged("Length");
                RaisePropertyChanged("Duration");
                RaisePropertyChanged("FormatLength");
                RaisePropertyChanged("BytesSize");
            }
            get { return this.length; }
        }

        public string FormatLength
        {
            get
            {
                if (length <= 0)
                {
                    return "0";
                }

                string[] names = new string[] { "b", "kb", "Mb", "Gb", "Tb" };

                double s = length * BytesPerSector;
                double pow = Math.Log(s, 2);
                int index = (int)Math.Floor(pow / 10);
                double pow2 = pow - index * 10;

                return Math.Round(Math.Pow(2, pow2), 2).ToString() + " " + names[index];
            }
        }

        public TimeSpan Duration
        {
            get { return new TimeSpan((long)(this.length / (double) SAMPLING_FREQUENCY * 22 * 10000000 )); }
        }

        public long BreakCount
        {
            set { this.breakCount = value; RaisePropertyChanged("BreakCount"); }
            get { return this.breakCount; }
        }

        public long CrcErrorCount
        {
            set { this.crcErrorCount = value; RaisePropertyChanged("CrcErrorCount"); }
            get { return this.crcErrorCount; }
        }

        public ulong BytesSize
        {
            get { return this.bytesSize; }
        }

        #endregion

        public uint BytesPerSector { get; set; }
    }
}
