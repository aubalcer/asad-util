﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ASADUtil.Service.Model
{
    [Serializable]
    public class UsbDevice : BaseModel
    {
        private struct StroredData
        {
            public UInt32 bytesPerSector;
        }

        public const byte EMPTY_BYTE = 0xff;
        public const int IO_BUFFER_SIZE = 65536;

        
        #region Private Properties

        private UInt64 totalSectors;
        private UInt32 bytesPerSector;
        private string deviceId;
        private ulong size;
        private string caption;
        
        private Range range;

        private Record record;

        #endregion

        public Range DiskRange
        {
            set { this.range = value; RaisePropertyChanged("DiskRange"); }
            get { return this.range; }
        }

        public Record DataRecord
        {
            set { this.record = value; RaisePropertyChanged("DataRecord"); }
            get { return this.record; }
        }

        public string Caption
        {
            set { this.caption = value; RaisePropertyChanged("Caption"); }
            get { return this.caption; }
        }

        public ulong Size
        {
            set { this.size = value; RaisePropertyChanged("Size"); RaisePropertyChanged("FormatSize"); }
            get { return this.size; }
        }


        public string FormatSize
        {
            get
            {
                if (size <= 0)
                {
                    return "";
                }

                string[] names = new string[] { "b", "kb", "Mb", "Gb", "Tb" };

                double s = size;
                double pow = Math.Log(s, 2);
                int index = (int)Math.Floor(pow / 10);
                double pow2 = pow - index * 10;

                return Math.Round(Math.Pow(2, pow2), 2).ToString() + " " + names[index];
            }
        }

        public UInt64 TotalSectors
        {
            set { this.totalSectors = value; RaisePropertyChanged("TotalSectors"); }
            get { return this.totalSectors; }
        }

        public UInt32 BytesPerSector
        {
            set
            {
                this.bytesPerSector = value;
                RaisePropertyChanged("BytesPerSector");
            }
            get { return this.bytesPerSector; }
        }


        public string DeviceId
        {
            set { this.deviceId = value; RaisePropertyChanged("DeviceId"); }
            get { return this.deviceId; }
        }

        public UsbDevice()
        {
            DeviceId = "";
            TotalSectors = 0;
            BytesPerSector = 0;
            Size = 0;
            
            DiskRange = new Range();
            DataRecord = new Record();
        }

        internal static UsbDevice fromQueryObj(System.Management.ManagementObject queryObj)
        {
            UsbDevice device = new UsbDevice();
            device.Caption = queryObj["Caption"].ToString();
            device.DeviceId = queryObj["DeviceID"].ToString();
            device.BytesPerSector = UInt32.Parse(queryObj["BytesPerSector"].ToString());
            device.TotalSectors = UInt64.Parse(queryObj["TotalSectors"].ToString());
            device.Size = device.TotalSectors * device.BytesPerSector;

            //Trace.WriteLine("Caption: " + queryObj["Caption"].ToString());
            //Trace.WriteLine("CreationClassName: " + queryObj["CreationClassName"].ToString());
            //Trace.WriteLine("Description: " + queryObj["Description"].ToString());
            //Trace.WriteLine("InterfaceType: " + queryObj["InterfaceType"].ToString());
            //Trace.WriteLine("Manufacturer: " + queryObj["Manufacturer"].ToString());
            //Trace.WriteLine("MediaType: " + queryObj["MediaType"].ToString());
            //Trace.WriteLine("Model: " + queryObj["Model"].ToString());
            //Trace.WriteLine("Name: " + queryObj["Name"].ToString());
            //Trace.WriteLine("PNPDeviceID: " + queryObj["PNPDeviceID"].ToString());
            //Trace.WriteLine("SystemCreationClassName: " + queryObj["SystemCreationClassName"].ToString());
            //Trace.WriteLine("SystemName: " + queryObj["SystemName"].ToString());

            //Trace.WriteLine("------------------------------------------------------------------");
            return device;
        }

        public void setDefaultRange()
        {
            this.DiskRange.CurrentPos = 0;
            this.DiskRange.StartPos = 0;
            this.DiskRange.EndPos = this.totalSectors;
        }

        public override string ToString()
        {
            if (Size == 0)
            {
                return Caption;
            }
            else
            {
                return String.Format("{0} ({1})", Caption, FormatSize);
            }
        }

        public void serialize(Stream stream)
        {
            //int size = Marshal.SizeOf(typeof(StroredData));
            //byte[] buffer = new byte[size];
            //IntPtr ptr = Marshal.AllocHGlobal(size);

            //StroredData data = new StroredData();
            //data.bytesPerSector = bytesPerSector;

            //Marshal.StructureToPtr(data, ptr, false);
            //Marshal.Copy(ptr, buffer, 0, size);

            //stream.Write(buffer, 0, size);
            //stream.Flush();

            //Marshal.FreeHGlobal(ptr);
        }

        public static UsbDevice deserialize(Stream stream, string fileName)
        {
            //int size = Marshal.SizeOf(typeof(StroredData));
            //byte[] buffer = new byte[size];
            //IntPtr ptr = Marshal.AllocHGlobal(size);

            //stream.Read(buffer, 0, size);
            //Marshal.Copy(buffer, 0, ptr, size);

            //StroredData data = (StroredData) Marshal.PtrToStructure(ptr, typeof(StroredData));

            //Marshal.FreeHGlobal(ptr);

            UsbDevice device = new UsbDevice();

            StroredData data = new StroredData();
            data.bytesPerSector = 512;

            device.BytesPerSector = data.bytesPerSector;
            device.DeviceId = fileName;
            device.TotalSectors = (ulong)(stream.Length / data.bytesPerSector);
            device.Size = (ulong) stream.Length;
            device.setDefaultRange();

            device.DataRecord.BytesPerSector = data.bytesPerSector;
            device.DataRecord.Length = device.TotalSectors;
            return device;
        }


        public static UsbDevice EmptyDevice
        {
            get
            {
                UsbDevice device = new UsbDevice();
                device.DeviceId = null;
                device.Caption = "Выберите";
                return device;
            }
        }

        public void setFillAreaRange(ulong size)
        {
            this.DiskRange.StartPos = 0;
            this.DiskRange.CurrentPos = 0;

            if (size > 0)
            {
                this.DiskRange.EndPos = size;
            }
        }

		public class Builder {
			private UsbDevice device;

			public static Builder Create(){
				Builder builder = new Builder();
				builder.device = new UsbDevice();
				return builder;
			}

			public Builder WithDiskRange(ulong start, ulong end) 
			{
				this.device.DiskRange = new Range(start, end);
				return this;
			}

			public Builder WithDataRecord(Record record) 
			{
				this.device.DataRecord = record;
				return this;
			}

			public Builder WithBytesPerSector(UInt32 bytesPerSector) 
			{
				this.device.BytesPerSector = bytesPerSector;
				return this;
			}

			public Builder WithTotalSectors(UInt32 totalSectors) 
			{
				this.device.TotalSectors = totalSectors;
				return this;
			}

			public Builder WithCaption(String caption) 
			{
				this.device.Caption = caption;
				return this;
			}

			public Builder WithSize(ulong size) 
			{
				this.device.Size = size;
				return this;
			}

			public Builder WithTotalSectors(ulong size) 
			{
				this.device.Size = size;
				return this;
			}

			public Builder WithDeviceId(String deviceId) 
			{
				this.device.DeviceId = deviceId;
				return this;
			}

			public UsbDevice Build() 
			{
				return this.device;
			}

		}
    }
}

