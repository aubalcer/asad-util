using ASADUtil.Service.Disk;
using System;
using System.Collections.Generic;


namespace ASADUtil.Service.Model
{
    public class StatusWord
    {
        public static int FULL_LENGTH = 32;
        public static int LENGTH = 8;
        public static int FREQUENCY = FULL_LENGTH / LENGTH;
		public const int ADC_CHANNEL_COUNT = 8;

        public Int32 Ku { get; set; }
        public Int32 Vrefm { get; set; }
        public float VRefMeas { get; set; }
        public Int32 Vtd { get; set; }
        public Int32 Rb { get; set; }
		public Int32 PowerGood { get; set; }
        public Int32 Tcpu { get; set; }
        public float Vsup { get; set; }
        public List<float> Vads { get; set; }
		public List<float> RSg { get; set; }
		public byte Order { get; set; }


        public StatusWord()
        {
            Ku = 100;
            Vrefm = 2500;
            Vtd = 3900;
            Rb = 30000;

            Vads = new List<float>();
			RSg = new List<float>();
        }


        public float getRSg(float qSg)
        {
            float r = (qSg != 0) ? (float)Rb * ((float)Vtd / (qSg * 1000) - 2) / 100 : 0;
            if (r > 1000) r = 1100;
            else if (r < -100) r = -200; 

            return r;
        }

		internal float getVsg(short uAdc)
        {
            return (Ku != 0) ? (float) (uAdc * Vrefm) / 32768 /(float) Ku : 0;
        }
    }
}
