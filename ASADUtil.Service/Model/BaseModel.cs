﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASADUtil.Service.Model
{
    public class BaseModel: INotifyPropertyChanged
    {
            protected bool isChange = false;
            public event PropertyChangedEventHandler PropertyChanged;

            protected void RaisePropertyChanged(string propertyName)
            {
                isChange = true;
                var e = PropertyChanged;
                if (e != null)
                {
                    e(this, new PropertyChangedEventArgs(propertyName));
                }
            }

    }
}
