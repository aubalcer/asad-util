﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASADUtil.Service.Model
{
    public class Channel
    {
        public string Name { get; set; }
        public string XUints { get; set; }
        public string Comment { get; set; }
        public string YUnits { get; set; }
        public string YFormat { get; set; }

        public int Total { get; set; }
        public int Divisor { get; set; }

        public string Mask { get; set; }

		public int Order { get; set; } //Flag for StatusWord it can be 0, 1, 2, 3
    }
}
