﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASADUtil.Service.Model
{
    public class Range : BaseModel
    {
        private ulong startPos;
        private ulong endPos;
        private ulong currentPos;

        public ulong StartPos
        {
            get { return startPos; }
            set { startPos = value; RaisePropertyChanged("StartPos"); }
        }
        public ulong EndPos
        {
            get { return endPos; }
            set { endPos = value; RaisePropertyChanged("EndPos"); }
        }
        public ulong CurrentPos
        {
            get { return currentPos; }
            set { currentPos = value; RaisePropertyChanged("CurrentPos"); }
        }

        public Range()
        {
            StartPos = 0;
            EndPos = 0;
            CurrentPos = 0;
        }

        public Range(ulong startPos, ulong endPos)
        {
            StartPos = startPos;
            EndPos = endPos;
            CurrentPos = 0;
        }

        public double getInterval()
        {
            return (currentPos == 0) ? 1 : ((double)(endPos - currentPos) / (currentPos - startPos));
        }

        public ulong GetSize()
        {
            return endPos - startPos;
        }

        internal double getProgress()
        {
            return (double)(currentPos - startPos) / (double)(endPos - startPos);
        }

        internal double getOffset()
        {
            return currentPos - startPos;
        }
    }
}
