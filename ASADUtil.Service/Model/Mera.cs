﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASADUtil.Service.Model
{
    public class Mera : BaseModel, IDataErrorInfo
    {
        public static int CHANNEL_COUNT = 8;
        public static string CHANNEL_NAME_FORMAT = "{0}(Sg{1})";
        
        public static List<Channel> Channels = new List<Channel>() {
			//Main channels
			new Channel(){Name = "tms_V", Comment="", XUints="сек.", YFormat="R4", YUnits="мВ", Total=Mera.CHANNEL_COUNT, Divisor = 1, Mask=Mera.CHANNEL_NAME_FORMAT, Order = -1},
			new Channel(){Name = "tms_S", Comment="", XUints="сек.", YFormat="R4", YUnits="Об/мин", Total=1, Divisor = SdData.ADC_FRAME_COUNT, Mask="{0}", Order = -1},

			//Additional channels
			new Channel(){Name = "T(cpu)", Comment="", XUints="сек.", YFormat="I4", YUnits="°C", Total=1, Divisor = SdData.ADC_FRAME_COUNT*4, Mask="{0}", Order = 0},

			new Channel(){Name = "tms_Q", Comment="", XUints="сек.", YFormat="R4", YUnits="В", Total=Mera.CHANNEL_COUNT, Divisor = SdData.ADC_FRAME_COUNT *4, Mask=Mera.CHANNEL_NAME_FORMAT, Order = 2},
			new Channel(){Name = "tms_R", Comment="", XUints="сек.", YFormat="R4", YUnits="Ом", Total=Mera.CHANNEL_COUNT, Divisor = SdData.ADC_FRAME_COUNT*4, Mask=Mera.CHANNEL_NAME_FORMAT, Order = 2},
            
			new Channel(){Name = "PowerGood", Comment="", XUints="сек.", YFormat="I4", YUnits="бр", Total=1, Divisor = SdData.ADC_FRAME_COUNT*4, Mask="{0}", Order = 3},
			new Channel(){Name = "Vsup", Comment="", XUints="сек.", YFormat="R4", YUnits="В", Total=1, Divisor = SdData.ADC_FRAME_COUNT*4, Mask="{0}", Order = 3},
			new Channel(){Name = "V(ref_meas)", Comment="", XUints="сек.", YFormat="R4", YUnits="В", Total=1, Divisor = SdData.ADC_FRAME_COUNT*4, Mask="{0}", Order = 3},
        };
        
        
        private string name;
        private string fileName;
        private string productName;
        private DateTime startDate;

        public Mera()
        {
            name = null;
            productName = null;
            startDate = DateTime.Now;
        }

        public string Name
        {
            set
            {
                if (name == value)
                {
                    return;
                }

                name = value;
                RaisePropertyChanged("Name");
            }
            get { return name; }
        }

        public string FileName
        {
            set
            {
                if (fileName == value)
                {
                    return;
                }

                fileName = value;
                RaisePropertyChanged("FileName");
            }
            get { return fileName; }
        }

        public string ProductName
        {
            set
            {
                productName = value;
                RaisePropertyChanged("ProductName");
            }
            get { return productName; }
        }

        public DateTime StartDate
        {
            set { startDate = value; RaisePropertyChanged("StartDate"); }
            get { return startDate; }
        }


        internal bool validate()
        {
            if (String.IsNullOrEmpty(Name))
            {
                return false;
            }

            if (String.IsNullOrEmpty(ProductName))
            {
                return false;
            }

            return true;
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string propertyName]
        {
            get
            {
                string validationResult = null;
                switch (propertyName)
                {
                    case "ProductName":
                        validationResult = validateProductName();
                        break;
                    case "Name":
                        validationResult = validateName();
                        break;
                    case "FileName":
                        validationResult = validateFileName();
                        break;
                    case "CreateDate":
                        validationResult = validateDate();
                        break;
                    default:
                        throw new ApplicationException("Unknown Property being validated on Product.");
                }
                return validationResult;
            }
        }

        private string validateFileName()
        {
            if (isChange && String.IsNullOrEmpty(FileName))
            {
                return "Заполните поле \"Имя файла\"";
            }
            else
            {
                return String.Empty;
            }
        }

        private string validateDate()
        {
            return String.Empty;
        }

        private string validateName()
        {
            if (isChange && String.IsNullOrEmpty(Name))
            {
                return "Заполните поле \"Наименование изделия\"";
            }
            else
            {
                return String.Empty;
            }
        }

        private string validateProductName()
        {
            if (isChange && String.IsNullOrEmpty(Name))
            {
                return "Заполните поле \"Название испытания\"";
            }
            else
            {
                return String.Empty;
            }
        }

		public class Builder 
		{
			Mera mera;

			public static Builder Create()
			{
				Builder builder = new Builder();
				builder.mera = new Mera();
				return builder;
			}


			public Builder WithName(String name) 
			{
				mera.Name = name;
				return this;
			}

			public Builder WithFileName(String fileName) 
			{
				mera.FileName = fileName;
				return this;
			}

			public Builder WithProductName(String productName) 
			{
				mera.ProductName = productName;
				return this;
			}

			public Builder WithStartDate(DateTime date) 
			{
				mera.StartDate = date;
				return this;
			}

			public Mera Build()
			{
				return this.mera;
			}

		}
    }
}
