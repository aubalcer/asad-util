﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASADUtil.Service.Model
{
    public class Progress : BaseModel
    {
        private DateTime startTime;
        private TimeSpan leftTime;
        private string velocity;
        private double rate;

        public TimeSpan LeftTime
        {
            get { return this.leftTime; }
            set { this.leftTime = value; RaisePropertyChanged("LeftTime"); }
        }

        public string Velocity
        {
            get { return velocity; }
            set { velocity = value; RaisePropertyChanged("Velocity"); }
        }

        public double Rate {
            get { return rate; }
            set { rate = value; RaisePropertyChanged("Rate"); }
        }

        public void Start()
        {
            this.startTime = DateTime.Now;
        }

        public void Update(Range range)
        {
            long interval = DateTime.Now.Ticks - startTime.Ticks;
            double totalTicks = (range.getInterval() * (double)interval);
            LeftTime = new TimeSpan((long)totalTicks);

            double v = (double)range.getOffset() * 512 * 10000000 / interval;
            if (v != 0)
            {
                string[] names = new string[] { "b/s", "kb/s", "Mb/s", "Gb/s", "Tb/s" };

                double pow = Math.Log(v, 2);
                int index = (int)Math.Floor(pow / 10);
                double pow2 = pow - index * 10;

                if (index >= 0 && index < names.Length)
                {
                    Velocity = Math.Round(Math.Pow(2, pow2), 2).ToString() + " " + names[index];
                }
                else
                {
                    Velocity = "Не определена";
                }
            }

            Rate = range.getProgress();
        }
    }
}
